// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    student, err := UnmarshalStudent(bytes)
//    bytes, err = student.Marshal()

package models

import "encoding/json"

func UnmarshalStudent(data []byte) (Student, error) {
	var r Student
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Student) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Student struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Age  int32  `json:"age"`
}
