// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    student, err := UnmarshalTest(bytes)
//    bytes, err = Test.Marshal()

package models

import "encoding/json"

func UnmarshalTest(data []byte) (Test, error) {
	var r Test
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Test) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Test struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
