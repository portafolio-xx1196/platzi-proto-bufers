package main

import (
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
	"xx1196/go/grpc/database"
	"xx1196/go/grpc/server"
	"xx1196/go/grpc/studentpb"
)

func main() {
	list, err := net.Listen("tcp", ":5060")
	fmt.Println("se esta iniciado el server de student")

	if err != nil {
		log.Fatalln(err)
	}

	repo, err := database.NewPostgresRepository("postgres://postgres:postgres@localhost:5432/postgres?sslmode=disable")

	studentServer := server.NewStudentServer(repo)

	if err != nil {
		log.Fatalln(err)
	}

	s := grpc.NewServer()

	studentpb.RegisterStudentServiceServer(s, studentServer)

	reflection.Register(s)

	fmt.Println("se ha iniciado el server de student")

	if err := s.Serve(list); err != nil {
		log.Fatal(err)
	}

}
