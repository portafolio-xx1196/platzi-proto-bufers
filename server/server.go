package server

import (
	"context"
	"xx1196/go/grpc/models"
	"xx1196/go/grpc/repository"
	"xx1196/go/grpc/studentpb"
)

type Server struct {
	repo repository.StudentRepository
	studentpb.UnimplementedStudentServiceServer
}

func NewStudentServer(repo repository.StudentRepository) *Server {
	return &Server{repo: repo}
}

func (s Server) GetStudent(ctx context.Context, req *studentpb.GetStudentRequest) (*studentpb.Student, error) {
	student, err := s.repo.GetStudent(ctx, req.GetId())

	if err != nil {
		return nil, err
	}

	return &studentpb.Student{
		Id:   student.ID,
		Name: student.Name,
		Age:  student.Age,
	}, nil
}

func (s Server) SetStudent(ctx context.Context, req *studentpb.Student) (*studentpb.SetStudentResponse, error) {
	student := &models.Student{
		ID:   req.GetId(),
		Name: req.GetName(),
		Age:  req.GetAge(),
	}

	err := s.repo.SetStudent(ctx, student)

	if err != nil {
		return nil, err
	}

	return &studentpb.SetStudentResponse{
		Id: student.ID,
	}, nil
}
