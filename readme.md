protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative studentpb/student.proto

protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative testpb/test.proto
# Ver todo los logs
docker-compose up

# Ocultar los logs en el background
docker-compose up -d


Crear el archivo .proto primero.
Compilarlo para generar los paquetes de Go.
Implementar el protobuffer a nivel de servidor, a nivel de base de datos y a nivel de interacción con gRPC.