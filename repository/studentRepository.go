package repository

import (
	"context"
	"xx1196/go/grpc/models"
)

type StudentRepository interface {
	GetStudent(ctx context.Context, id string) (*models.Student, error)
	SetStudent(ctx context.Context, student *models.Student) error
}

var studentImplementation StudentRepository

func setStudentRepository(studentRepository StudentRepository) {
	studentImplementation = studentRepository
}

func GetStudent(ctx context.Context, id string) (*models.Student, error) {
	return studentImplementation.GetStudent(ctx, id)
}
func SetStudent(ctx context.Context, student *models.Student) error {
	return studentImplementation.SetStudent(ctx, student)
}
