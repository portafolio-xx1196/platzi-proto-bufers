package repository

import (
	"context"
	"xx1196/go/grpc/models"
)

type TestRepository interface {
	GetTest(ctx context.Context, id string) (*models.Test, error)
	SetTest(ctx context.Context, test *models.Test) error
	SetQuestion(ctx context.Context, question *models.Question) error
	GetStudentsPerTest(ctx context.Context, testId string) ([]*models.Student, error)
	SetEnrollment(ctx context.Context, enrollment *models.Enrollment) error
	GetQuestionsPerTest(ctx context.Context, testId string) ([]*models.Question, error)
}

var testImplementation TestRepository

func setTestRepository(testRepository TestRepository) {
	testImplementation = testRepository
}

func GetTest(ctx context.Context, id string) (*models.Test, error) {
	return testImplementation.GetTest(ctx, id)
}

func SetTest(ctx context.Context, test *models.Test) error {
	return testImplementation.SetTest(ctx, test)
}

func SetQuestion(ctx context.Context, question *models.Question) error {
	return testImplementation.SetQuestion(ctx, question)
}

func GetStudentsPerTest(ctx context.Context, testId string) ([]*models.Student, error) {
	return testImplementation.GetStudentsPerTest(ctx, testId)
}

func SetEnrollment(ctx context.Context, enrollment *models.Enrollment) error {
	return testImplementation.SetEnrollment(ctx, enrollment)
}

func GetQuestionsPerTest(ctx context.Context, testId string) ([]*models.Question, error) {
	return testImplementation.GetQuestionsPerTest(ctx, testId)
}
