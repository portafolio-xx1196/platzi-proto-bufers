package main

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"time"
	"xx1196/go/grpc/testpb"
)

func main() {
	cc, err := grpc.Dial("localhost:5070", grpc.WithTransportCredentials(
		insecure.NewCredentials(),
	))

	if err != nil {
		log.Fatalf("No se pudo conectar: %v", err)
	}

	defer func(cc *grpc.ClientConn) {
		err := cc.Close()
		if err != nil {
			log.Fatalf("No se pudo cerrar: %v", err)
		}
	}(cc)

	c := testpb.NewTestServiceClient(cc)

	DoUnary(c)
	DoClientStreaming(c)
}

func DoUnary(c testpb.TestServiceClient) {
	req := &testpb.GetTestRequest{
		Id: "t1",
	}

	res, err := c.GetTest(context.Background(), req)

	if err != nil {
		log.Fatalf("Hubo un error: %v", err)
	}

	log.Printf("Se respondio con %v", res)
}

func DoClientStreaming(c testpb.TestServiceClient) {
	questions := []*testpb.Question{
		{
			Id:       "Q4T1",
			Answer:   "azul",
			Question: "Hola",
			TestId:   "t1",
		},
	}
	stream, err := c.SetQuestions(context.Background())

	if err != nil {
		log.Fatalf("Hubo un error llamando SetQuestions: %v", err)
	}

	for _, question := range questions {
		log.Println("enviando el question:", question.Id)

		err := stream.Send(question)
		if err != nil {
			log.Fatalf("Hubo un error enviando el question: %v", question)
		}
		time.Sleep(5 * time.Second)
	}

	msg, err := stream.CloseAndRecv()

	if err != nil {
		log.Fatalf("Hubo un error cerrando el stream: %v", err)
	}

	log.Printf("respondio: %v", msg)

}
