package main

import (
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
	"xx1196/go/grpc/database"
	"xx1196/go/grpc/server"
	"xx1196/go/grpc/testpb"
)

func main() {
	list, err := net.Listen("tcp", ":5070")
	fmt.Println("se esta iniciado el server de test")

	if err != nil {
		log.Fatalln(err)
	}

	repo, err := database.NewPostgresRepository("postgres://postgres:postgres@localhost:5432/postgres?sslmode=disable")

	testServer := server.NewTestServer(repo)

	if err != nil {
		log.Fatalln(err)
	}

	s := grpc.NewServer()

	testpb.RegisterTestServiceServer(s, testServer)

	reflection.Register(s)

	fmt.Println("se ha iniciado el server de test")

	if err := s.Serve(list); err != nil {
		log.Fatal(err)
	}
}
